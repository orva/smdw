use assert_cmd::prelude::*;
use assert_fs::{fixture::ChildPath, prelude::*, TempDir};
use predicates as p;
use predicates::prelude::*;
use scraper::{Html, Selector};
use std::{path::Path, process::Command};

#[test]
fn nested_directories_convert_as_expected() {
    let content_dir = generate_content_directory();
    let output_dir = assert_fs::TempDir::new().unwrap();
    run(&content_dir, &output_dir);

    output_dir.child("content.html").assert(p::path::exists());
    output_dir
        .child("subdir-1/nested-content-1.html")
        .assert(p::path::exists());
    output_dir
        .child("subdir-2/nested-content-2.html")
        .assert(p::path::exists());
    output_dir
        .child("subdir-2/subdir-3/nested-content-3.html")
        .assert(p::path::exists());
    output_dir
        .child("smdw-assets/styles.css")
        .assert(p::path::exists());
}

#[test]
fn converted_files_are_in_correct_locations() {
    let content_dir = generate_content_directory();
    let output_dir = assert_fs::TempDir::new().unwrap();
    run(&content_dir, &output_dir);

    assert_content(&output_dir, "content.md");
    assert_content(&output_dir, "subdir-1/nested-content-1.md");
    assert_content(&output_dir, "subdir-2/nested-content-2.md");
    assert_content(&output_dir, "subdir-2/subdir-3/nested-content-3.md");
}

#[test]
fn internal_css_asset_path_is_relative_and_correct() {
    let content_dir = generate_content_directory();
    let output_dir = assert_fs::TempDir::new().unwrap();
    run(&content_dir, &output_dir);

    assert_eq!(
        css_links(output_dir.child("content.html")).get(0).unwrap(),
        "smdw-assets/styles.css"
    );
    assert_eq!(
        css_links(output_dir.child("subdir-1/nested-content-1.html"))
            .get(0)
            .unwrap(),
        "../smdw-assets/styles.css"
    );
    assert_eq!(
        css_links(output_dir.child("subdir-2/nested-content-2.html"))
            .get(0)
            .unwrap(),
        "../smdw-assets/styles.css"
    );
    assert_eq!(
        css_links(output_dir.child("subdir-2/subdir-3/nested-content-3.html"))
            .get(0)
            .unwrap(),
        "../../smdw-assets/styles.css"
    );
}

#[test]
fn md_to_html_link_conversion_ignores_external_links() {
    let content_dir = generate_content_directory();
    gen_links(&content_dir);
    let output_dir = assert_fs::TempDir::new().unwrap();
    run(&content_dir, &output_dir);

    let link_content_path = output_dir.child("subdir-2/link-content.html");
    assert_eq!(
        get_link(&link_content_path, "external inline https link"),
        "https://example.com/inline.md"
    );
    assert_eq!(
        get_link(&link_content_path, "external inline http link"),
        "http://example.com/inline.md"
    );
    assert_eq!(
        get_link(&link_content_path, "external reference https link"),
        "https://example.com/reference.md"
    );
    assert_eq!(
        get_link(&link_content_path, "external reference http link"),
        "http://example.com/reference.md"
    );
    assert_eq!(
        get_link(&link_content_path, "http://example.com/autolink.md"),
        "http://example.com/autolink.md"
    );
    assert_eq!(
        get_link(&link_content_path, "https://example.com/autolink.md"),
        "https://example.com/autolink.md"
    );
}

#[test]
fn md_to_html_link_conversion() {
    let content_dir = generate_content_directory();
    gen_links(&content_dir);
    let output_dir = assert_fs::TempDir::new().unwrap();
    run(&content_dir, &output_dir);

    let link_content_path = output_dir.child("subdir-2/link-content.html");
    assert_eq!(
        get_link(&link_content_path, "internal inline md link"),
        "../subdir-1/inline-md-target.html"
    );
    assert_eq!(
        get_link(&link_content_path, "internal inline html link"),
        "../subdir-1/inline-html-target.html"
    );
    assert_eq!(
        get_link(&link_content_path, "internal reference md link"),
        "../subdir-1/reference-md-target.html"
    );
    assert_eq!(
        get_link(&link_content_path, "internal reference html link"),
        "../subdir-1/reference-html-target.html"
    );
}

#[test]
fn asset_copying() {
    let content_dir = generate_content_directory();
    gen_assets(&content_dir);
    let output_dir = assert_fs::TempDir::new().unwrap();
    run(&content_dir, &output_dir);

    output_dir
        .child("subdir-2/image.jpg")
        .assert(p::path::exists());
    output_dir
        .child("subdir-2/subdir-3/subdir-image.jpg")
        .assert(p::path::exists());
    output_dir
        .child("parent-image.jpg")
        .assert(p::path::exists());
    output_dir
        .child("subdir-1/adjacent-image.jpg")
        .assert(p::path::exists());
    output_dir
        .child("subdir-2/multi-use.jpg")
        .assert(p::path::exists());
    output_dir
        .child("do-not-copy-0.jpg")
        .assert(p::path::exists().not());
    output_dir
        .child("subdir-1/do-not-copy-1.jpg")
        .assert(p::path::exists().not());
    output_dir
        .child("subdir-2/do-not-copy-2.jpg")
        .assert(p::path::exists().not());
    output_dir
        .child("subdir-2/subdir-3/do-not-copy-3.jpg")
        .assert(p::path::exists().not());
}

fn run(content: &TempDir, output: &TempDir) {
    let mut cmd = Command::cargo_bin(env!("CARGO_PKG_NAME")).unwrap();
    cmd.arg("--output");
    cmd.arg(output.path());
    cmd.arg(content.path());
    cmd.unwrap();
}

fn generate_content_directory() -> assert_fs::TempDir {
    let content_dir = assert_fs::TempDir::new().unwrap();
    gen_plain(&content_dir, "content.md");
    gen_plain(&content_dir, "subdir-1/nested-content-1.md");
    gen_plain(&content_dir, "subdir-2/nested-content-2.md");
    gen_plain(&content_dir, "subdir-2/subdir-3/nested-content-3.md");
    content_dir
}

fn gen_plain(root: &TempDir, path: &str) {
    let content = format!(
        "
# Header for {}

Some content for {}
        ",
        path, path
    );

    let out = root.child(path);
    out.write_str(&content.trim()).unwrap();
}

fn gen_links(root: &TempDir) {
    gen_plain(&root, "subdir-1/inline-md-target.md");
    gen_plain(&root, "subdir-1/inline-html-target.md");
    gen_plain(&root, "subdir-1/reference-md-target.md");
    gen_plain(&root, "subdir-1/reference-html-target.md");

    let content = format!(
        "
# Header for link-test-page

- [external inline https link](https://example.com/inline.md)
- [external inline http link](http://example.com/inline.md)
- [external reference https link][external-reference-https-link]
- [external reference http link][external-reference-http-link]
- <https://example.com/autolink.md>
- <http://example.com/autolink.md>

- [internal inline md link](../subdir-1/inline-md-target.md)
- [internal inline html link](../subdir-1/inline-html-target.html)
- [internal reference md link][internal-reference-md-link]
- [internal reference html link][internal-reference-html-link]

[external-reference-https-link]: https://example.com/reference.md
[external-reference-http-link]: http://example.com/reference.md
[internal-reference-md-link]: ../subdir-1/reference-md-target.md
[internal-reference-html-link]: ../subdir-1/reference-html-target.md
        "
    );

    let out = root.child("subdir-2/link-content.md");
    out.write_str(&content.trim()).unwrap();
}

fn gen_assets(root: &TempDir) {
    let content = format!(
        "
# Header for asset-test-page

![image in same directory](./image.jpg)
![image in subdir](./subdir-3/subdir-image.jpg)
![image in parent dir](../parent-image.jpg)
![image in adjacent dir](../subdir-1/adjacent-image.jpg)
![image used first time](./multi-use.jpg)
![image used second time](./multi-use.jpg)
        "
    );

    let out = root.child("subdir-2/asset-content.md");
    out.write_str(&content.trim()).unwrap();

    // Generate some files that at least have expected file extension, even if
    // content is totally invalid.
    dummy_file(root, "subdir-2/image.jpg");
    dummy_file(root, "subdir-2/multi-use.jpg");
    dummy_file(root, "subdir-2/subdir-3/subdir-image.jpg");
    dummy_file(root, "parent-image.jpg");
    dummy_file(root, "subdir-1/adjacent-image.jpg");
    dummy_file(root, "do-not-copy-0.jpg");
    dummy_file(root, "subdir-1/do-not-copy-1.jpg");
    dummy_file(root, "subdir-2/do-not-copy-2.jpg");
    dummy_file(root, "subdir-2/subdir-3/do-not-copy-3.jpg");
}

fn dummy_file(root: &TempDir, path: &str) {
    let out = root.child(path);
    out.write_str("dummy content").unwrap();
}

fn get_link(html_path: &ChildPath, text: &str) -> String {
    let content = std::fs::read_to_string(html_path).unwrap();
    let doc = Html::parse_document(&content);
    let link_selector = Selector::parse("a").unwrap();
    doc.select(&link_selector)
        .find(|el| {
            let link_text = el.text().nth(0).unwrap();
            link_text == text
        })
        .and_then(|el| el.attr("href"))
        .unwrap()
        .to_string()
}

fn assert_content(tmpdir: &TempDir, content_subpath: &str) {
    let html = Path::new(content_subpath).with_extension("html");
    tmpdir
        .child(html)
        .assert(p::str::contains(format!("Header for {}", content_subpath)))
        .assert(p::str::contains(format!(
            "Some content for {}",
            content_subpath
        )));
}

fn css_links(html_path: ChildPath) -> Vec<String> {
    let content = std::fs::read_to_string(html_path).unwrap();
    let doc = Html::parse_document(&content);
    let stylesheet_selector = Selector::parse("link[rel='stylesheet']").unwrap();
    doc.select(&stylesheet_selector)
        .map(|el| el.value().attr("href").unwrap().to_string())
        .collect()
}
