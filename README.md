# smdw - Simple Markdown Wiki

Rather opinionated and simple way of turning plain wiki-ish markdown files into
a hostable HTML site. No templates, no customization. Just markdown files in
folders and single binary to do the conversion.

Well, there is (going to be) possibility for styling. But that's it.

## Documentation and guides

Documentation can be found online at <https://orva.codeberg.page/smdw/>.
Generated from `docs` directory.

## Changelog

See `docs/release-notes.md` or <https://orva.codeberg.page/smdw/release-notes.html>.

## License

This project is licensed under
[GPL-3.0-only](https://spdx.org/licenses/GPL-3.0-only.html).
