use pulldown_cmark::{CowStr, Event, HeadingLevel, Options, Parser as MarkdownParser, Tag, TagEnd};
use std::path::PathBuf;

pub struct Parser<'input> {
    pub title: Option<String>,
    pub asset_refs: Vec<PathBuf>,
    inner: MarkdownParser<'input>,
    state: ParseState,
}

#[derive(PartialEq)]
enum ParseState {
    Initial,
    TitleOpen,
    TitleCollected,
}

impl<'input> Parser<'input> {
    pub fn new(content: &'input str) -> Parser {
        let mut parser_opts = Options::empty();
        parser_opts.insert(Options::ENABLE_TABLES);
        parser_opts.insert(Options::ENABLE_FOOTNOTES);
        parser_opts.insert(Options::ENABLE_STRIKETHROUGH);
        parser_opts.insert(Options::ENABLE_TASKLISTS);

        Parser {
            inner: MarkdownParser::new_ext(content, parser_opts),
            state: ParseState::Initial,
            title: None,
            asset_refs: Vec::new(),
        }
    }

    fn handle_document_title_collection(&mut self, event: &Event<'_>) {
        match self.state {
            ParseState::Initial => {
                if let Event::Start(Tag::Heading {
                    level: HeadingLevel::H1,
                    ..
                }) = event
                {
                    self.state = ParseState::TitleOpen;
                }
            }
            ParseState::TitleOpen => match event {
                Event::Text(ref txt) => {
                    self.title = Some(txt.to_string());
                }
                Event::End(TagEnd::Heading(HeadingLevel::H1)) => {
                    self.state = ParseState::TitleCollected;
                }
                _ => {}
            },
            ParseState::TitleCollected => {}
        };
    }

    fn handle_link_operations(&mut self, event: &Event<'input>) -> Option<Event<'input>> {
        let Event::Start(Tag::Link {
            link_type,
            dest_url,
            title,
            id,
        }) = event
        else {
            return None;
        };

        if self.is_remote_link(dest_url) {
            return None;
        }

        let md_extensions = [".md", ".markdown"];
        match md_extensions.iter().find(|ext| dest_url.ends_with(*ext)) {
            None => None,
            Some(ext) => {
                let new_url = dest_url.replace(ext, ".html");
                let new_link = Tag::Link {
                    link_type: *link_type,
                    dest_url: new_url.into(),
                    title: title.clone(),
                    id: id.clone(),
                };
                Some(Event::Start(new_link))
            }
        }
    }

    fn handle_asset_link_collection(&mut self, event: &Event<'input>) {
        let Event::Start(Tag::Image { dest_url, .. }) = event else {
            return;
        };

        if self.is_remote_link(dest_url) {
            return;
        }

        self.asset_refs.push(dest_url.as_ref().into());
    }

    fn is_remote_link(&self, url: &CowStr<'_>) -> bool {
        // We probably want to match with /(.+\:\/\/).+/ to catch.. ftp links?
        url.starts_with("http://") || url.starts_with("https://")
    }
}

impl<'input> Iterator for Parser<'input> {
    type Item = Event<'input>;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|event| {
            self.handle_document_title_collection(&event);
            self.handle_asset_link_collection(&event);

            if let Some(edited_event) = self.handle_link_operations(&event) {
                return edited_event;
            }

            event
        })
    }
}
