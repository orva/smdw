mod converter;
mod parser;

use clap::Parser;
use converter::Converter;
use std::path::PathBuf;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Content directory with markdown files
    #[arg(value_name = "content directory")]
    content: PathBuf,

    /// Output directory for rendered HTML wiki
    #[arg(short, long, value_name = "directory", default_value = "./site")]
    output: PathBuf,
}

fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();
    let c = Converter::new(cli.content, cli.output);
    c.convert()
}
