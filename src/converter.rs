use crate::parser::Parser;
use anyhow::Context;
use handlebars::Handlebars;
use pulldown_cmark::html::push_html;
use rust_embed::RustEmbed;
use std::{
    fs::File,
    io::BufWriter,
    path::{Component, Path, PathBuf},
};

pub struct Converter {
    input_directory: PathBuf,
    output_directory: PathBuf,
    // Invariant: we need this to be under `output_directory`
    asset_output_directory: PathBuf,
}

impl Converter {
    pub fn new(input_directory: PathBuf, output_directory: PathBuf) -> Converter {
        let asset_output_directory = output_directory.join("smdw-assets");
        Converter {
            input_directory,
            output_directory,
            asset_output_directory,
        }
    }

    pub fn convert(&self) -> anyhow::Result<()> {
        let templates = get_templates()?;

        let inputs = self.input_tree(&self.input_directory)?;
        self.assert_output_directory()?;

        for (dir, input_files) in inputs {
            std::fs::create_dir_all(self.output_directory.join(&dir))?;

            let asset_prefix = self.relative_path_to_asset_dir(&dir);
            let css_paths = css_asset_paths()
                .map(|css_path| asset_prefix.join(css_path))
                .collect();

            for file in input_files {
                let output = self.output_path(&file).map(|p| p.with_extension("html"));
                let Some(dest) = output else {
                    continue;
                };

                let content_markdown = std::fs::read_to_string(&file)?;
                let mut parser = Parser::new(&content_markdown);
                let mut content = String::new();
                push_html(&mut content, &mut parser);

                let out = BufWriter::new(File::create(dest)?);
                let data = PageTemplateParams {
                    content,
                    title: parser.title.unwrap_or("Simple Markdown Wiki".to_string()),
                    css_paths: &css_paths,
                };

                templates.render_to_write("page.html.hbs", &data, out)?;

                for asset_ref in parser.asset_refs {
                    let asset_path = dir.join(&asset_ref);
                    let asset = self.input_directory.join(&asset_path);
                    let out = self.output_directory.join(&asset_path);
                    std::fs::copy(asset, out)?;
                }
            }
        }

        std::fs::create_dir(&self.asset_output_directory)
            .with_context(|| "Could not create asset output directory")?;

        for asset_name in get_asset_names() {
            let asset = SmdwAsset::get(&asset_name)
                .ok_or_else(|| anyhow::anyhow!("Could not find asset {}", &asset_name))?;
            let asset_path = self.asset_output_directory.join(&asset_name);

            std::fs::write(asset_path, asset.data)
                .with_context(|| anyhow::anyhow!("Could not write asset {}", &asset_name))?;
        }

        Ok(())
    }

    fn output_path(&self, input_file: &Path) -> Option<PathBuf> {
        input_file
            .strip_prefix(&self.input_directory)
            .ok()
            .map(|fname| self.output_directory.join(fname))
    }

    fn relative_path_to_asset_dir(&self, subdir: &Path) -> PathBuf {
        let prefix: PathBuf = Path::new(subdir)
            .components()
            .map_while(|c| match c {
                Component::Normal(_) => Some(".."),
                _ => None,
            })
            .collect::<Vec<&str>>()
            .join("/")
            .into();

        prefix.join("smdw-assets")
    }

    fn input_tree(&self, dir: &PathBuf) -> anyhow::Result<Vec<(PathBuf, Vec<PathBuf>)>> {
        let mut files: Vec<PathBuf> = Vec::new();
        let mut subdirs: Vec<(PathBuf, Vec<PathBuf>)> = Vec::new();
        for entry in std::fs::read_dir(dir)? {
            let path = entry?.path();

            if path.is_dir() {
                let mut subdir = self.input_tree(&path)?;
                subdirs.append(&mut subdir);
                continue;
            }

            match path.extension().and_then(|ext| ext.to_str()) {
                None => continue,
                Some(ext) => match ext {
                    "md" | "markdown" => files.push(path),
                    _ => continue,
                },
            };
        }

        let relative_to_output = dir.strip_prefix(&self.input_directory)?;
        subdirs.push((relative_to_output.to_path_buf(), files));
        Ok(subdirs)
    }

    fn assert_output_directory(&self) -> anyhow::Result<()> {
        if self.output_directory.try_exists()? {
            let file_count = std::fs::read_dir(&self.output_directory)?.count();
            if file_count > 0 {
                return Err(anyhow::anyhow!(format!(
                    "output-directory {:?} already exists and is not empty",
                    &self.output_directory
                )));
            }
        } else {
            std::fs::create_dir(&self.output_directory)?;
        }

        Ok(())
    }
}

#[derive(RustEmbed)]
#[folder = "src/assets"]
struct SmdwAsset;

#[derive(serde::Serialize)]
struct PageTemplateParams<'a> {
    content: String,
    title: String,
    css_paths: &'a Vec<PathBuf>,
}

fn get_templates<'a>() -> anyhow::Result<Handlebars<'a>> {
    let template_names = SmdwAsset::iter().filter(|filename| {
        let asset_path = Path::new(filename.as_ref());
        match asset_path.extension() {
            None => false,
            Some(ext) => ext == "hbs",
        }
    });

    let mut templates = Handlebars::new();
    for tn in template_names {
        let template_name = tn.as_ref();
        let template_asset = SmdwAsset::get(template_name)
            .ok_or(anyhow::anyhow!("Could not find asset ({})", template_name))?;
        let template_contents = std::str::from_utf8(&template_asset.data)
            .with_context(|| format!("Page template ({}) was not valid utf-8", template_name))?;
        templates.register_template_string(template_name, template_contents)?;
    }

    Ok(templates)
}

fn css_asset_paths() -> impl Iterator<Item = PathBuf> {
    get_asset_names().filter_map(|asset_name| {
        let p = Path::new(&asset_name);
        p.extension().and_then(|ext| {
            if ext != "css" {
                return None;
            }

            Some(p.to_path_buf())
        })
    })
}

fn get_asset_names() -> impl Iterator<Item = String> {
    SmdwAsset::iter().filter_map(|filename| {
        let asset_name = filename.as_ref();
        let p = Path::new(asset_name);
        p.extension().and_then(|ext| {
            // Do not output handlebar templates into places that expect static assets
            if ext == "hbs" {
                return None;
            }

            Some(asset_name.to_string())
        })
    })
}
