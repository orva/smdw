# Simple Markdown Wiki

Rather opinionated and simple way of turning markdown files into a wiki-ish
website. No templates, no customization. Just markdown files in directories and
a single CLI tool for HTML conversion. 

Well, there is going to be a possibility to add own styling easily. But that's
it.

## Introduction

Goal for this tool is to be as much content centric as possible, where content
is [markdown files](markdown.md). Assets referenced from the content (images,
audio, video) should be easily rendered. Authoring workflow should be without
adding formatting information, adding metadata blocks and should not require
tweaking templates.

This lack of customization is a blessing and a curse, as it does mean that the
tool doing the HTML conversion needs to have some opinions how resulting site
is going to be rendered. The blessing is that when you are happy with the
opinions, you just write your content without needing to think about
presentation. On the other hand, whole system feels cursed if you are not happy
with the authoring workflow.

As there is no content generator / templating workflow, structuring the content
is left for the user. There also is no support for automatically adding
metadata (creation dates, modification dates, etc.) to the output. This
naturally leans towards wiki-like content structure where linking between
documents is done by hand and documents are more like "living documents" that
evolve over time.

Hopefully you are going to be happy with this tool.

## Planned features

List of planned and not-planned features can be found in [todo](todo.md).

## Markdown

## Use cases

## Styling

## Generated content

## Release notes

Release notes are available in [release notes](release-notes.md)

