# Markdown showcase

This page should eventually contain examples of all the markdown elements which
are supported. For general "what is markdown" explanation and tutorial we can
recommend [markdown guide][md-guide].

[md-guide]: https://www.markdownguide.org/ (Markdown guide)

## Headings and their use

There should be only one `h1 (#)` level heading at the document (and it should
probably be the first non-whitespace thing in the file). It looks like the
title of this page.

This body of text is under `h2 (##)` level heading.

### Heading level `h3 (###)`

Level 3 heading loses the content dividing underline, making it useful for
dividing body of text into smaller pieces but still keeping it part of the
surrounding context.

As further we go, less prominent and less important the headers are going to
be. Rest of the heading levels are presented below.

#### Heading level `h4 (####)` 

##### Heading level `h5 (#####)`

###### Heading level `h6 (######)`


## Links

Links are important in webpages, and those are especially important in wiki-style
content where linking between content is one of the core concepts. Thus, we try
to make linking as smooth as possible and support following link styles:

- The usual [inline link](https://www.markdownguide.org/basic-syntax/#links "inline link to markdownguide")
- [Reference style][reference link] links
- Shortcut form of the [reference link]
- Autolinks to external sites <https://www.markdownguide.org/basic-syntax/#urls-and-email-addresses>


To make linking between documents easier while writing content, internal links
pointing to markdown files are converted automatically.

- [Inline markdown file link](./index.md) produces link to `./index.html`
- [Reference markdown file link][markdown reference link] also produces link to `./index.html`

[reference link]: https://www.markdownguide.org/basic-syntax/#reference-style-links "reference style link to markdownguide"
[markdown reference link]: ./index.md "reference style link to index.md"

That link example collection looks like this in Markdown:

```markdown
- The usual [inline link](https://www.markdownguide.org/basic-syntax/#links "inline link to markdownguide")
- [Reference style][reference link] links
- Shortcut form of the [reference link]
- Autolinks <https://www.markdownguide.org/basic-syntax/#urls-and-email-addresses>

- [Inline markdown file link](./index.md) produces link to `./index.html`
- [Reference markdown file link][markdown reference link] also produces link to `./index.html`

[reference link]: https://www.markdownguide.org/basic-syntax/#reference-style-links "reference style link to markdownguide"
[markdown reference link]: ./index.md "reference style link to index.md"
```

## Text content

Most common text element is obviously the venerable paragraph. You have seen
plenty of those already. Links (like [this one to main page](index.md))
should also be familiar already. Strike through, ~like this~, is supported. As
is `inline code blocks`, __bold text__ and _even italics_!

### Code blocks

Technical writing, nerdy blogging, etc. can require showing code and every piece
of code deserves to be beautiful. Or at least as beautiful it can be without
extensive syntax highlighting. As an example: one of the earliest incarnations
of the markdown to HTML conversion in this tool looked liked this:

```rust
for (dir, input_files) in self.input_tree(&self.input_directory)? {
    std::fs::create_dir_all(self.output_directory.join(&dir))?;

    let asset_prefix = self.relative_path_to_asset_dir(&dir);
    let css_paths = css_asset_paths()
        .map(|css_path| asset_prefix.join(css_path))
        .collect();

    for file in input_files {
        let dest =
            file.extension()
                .and_then(|ext| ext.to_str())
                .and_then(|ext| match ext {
                    "md" | "markdown" => self.output_path(&file).and_then(|fname| {
                        Some(ContentType::Markdown(fname.with_extension("html")))
                    }),
                    _ => self
                        .output_path(&file)
                        .and_then(|fname| Some(ContentType::Asset(fname))),
                });

        match dest {
            None => continue,
            Some(ContentType::Asset(_)) => continue,
            Some(ContentType::Markdown(dest)) => {
                let content_markdown = std::fs::read_to_string(&file)?;
                let parser = Parser::new_ext(&content_markdown, parser_opts);
                let mut content = String::new();
                push_html(&mut content, parser);

                let out = BufWriter::new(File::create(dest)?);
                let data = PageTemplateParams {
                    content,
                    css_paths: &css_paths,
                };

                templates.render_to_write("page.html.hbs", &data, out)?;
            }
        }
    }
}
```


### blockquotes

No one is going to produce all the world's wisdom using new words, so quoting
someone else is going to be needed. For this we have block quotes, which have
surprisingly long history according to
[Wikipedia](https://en.wikipedia.org/wiki/Block_quotation):

> In the first centuries of typesetting, quotations were distinguished merely
> by indicating the speaker, and this can still be seen on some editions of the
> Bible. During the Renaissance, quotations were distinguished by setting in a
> typeface contrasting with the main body text (often Italic type with roman,
> or the other way round). Block quotations were set this way at full size and
> full measure.
>
> Quotation marks were first cut in type during the middle of the sixteenth
> century, and were used copiously by some printers by the seventeenth. In
> Baroque and Romantic-period books, they could be repeated at the beginning of
> every line of a long quotation. When this practice was abandoned, the empty
> margin remained, leaving an indented block quotation.
>
> Quotation marks were first cut in type during the middle of the sixteenth
> century, and were used copiously by some printers by the seventeenth. In
> Baroque and Romantic-period books, they could be repeated at the beginning of
> every line of a long quotation. When this practice was abandoned, the empty
> margin remained, leaving an indented block quotation.

At least currently there is no way of adding a subtext inside the block
quotation, so information about the quote has to be written as normal
paragraph.

## Lists

Listing things is also surprisingly powerful tool when jotting down notes in a
hurry. Task lists are also rather useful tool, enough so that people might even
pay money for more advanced task list applications. We only provide lists
fitting for simple use.

### Plain lists

Lists that have no numbering that would signify their ordering. Can be nested
inside other lists.

- First item
- Second item
    - First sub-item for the second item
    - Another sub-item for the second item
        - Even deeper nesting
        - Second deeper list item
- Third item which is not nested

###### Markdown notation for the example above

```markdown
- First item
- Second item
    - First sub-item for the second
    - Second sub-item for the second
        - Even deeper nesting
        - Second deeper list item
- Third item which is unnested
```

### numbered lists

Lists that have numbering can also be nested. Note that nested numbering always
starts from 1, which can be slightly limiting. At least there less chance of
running out of numbers!

1. First item
1. Second item
    1. First sub-item for the second item
    1. Another sub-item for the second item
        1. Even deeper nesting
        1. Second deeper list item
    1. Nested numbered thing
    1. Nested numbered thing
1. Third item which is not nested

###### Markdown notation for the example above

```markdown
1. First item
1. Second item
    1. First sub-item for the second item
    1. Another sub-item for the second
        1. Even deeper nesting
        1. Second deeper list item
    1. Nested numbered thing
    1. Nested numbered thing
1. Third item which is unnested
```

### task lists

- [x] Decide to make new blog
- [ ] ~Choose static site generator~
- [ ] Write new static site generator
- [ ] Start a blog
- [ ] Write some blog posts
    - [ ] ~post about choosing a static site generator~
    - [ ] post about writing a static site generator

###### Markdown notation for the example above

```markdown
- [x] Decide to make new blog
- [ ] ~Choose static site generator~
- [ ] Write new static site generator
- [ ] Start a blog
- [ ] Write some blog posts
    - [ ] ~post about choosing a static site generator~
    - [ ] post about writing a static site generator
```

## Images

Images can be added as expected. Below should be serene image of sailing dinghy
on calm waters, found from
[Pixabay](https://pixabay.com/photos/boat-sailboat-lake-water-nature-933055/).

![Close up image of white sailing dinghy on a calm lake](./assets/dinghy.jpg "example image")

## Tables

Even though these tables styles are fairly basic they can be used for showing
few narrow columns of data. Note that these are not heavily styled
tables, so they don't scroll horizontally and _will_ break the layout on
smaller screen sizes when content is wide enough.

| Planet  | Moons | Atmosphere  |
|---------|-------|-------------|
| Mercury | 0     | minimal     |
| Venus   | 0     | CO2, N2     |
| Earth   | 1     | N2, O2, Ar  |
| Mars    | 2     | CO2, N2, Ar |
| Jupiter | 95    | H2, He      |
| Saturn  | 145   | H2, He      |
| Uranus  | 28    | H2, He, CH4 |
| Neptune | 16    | H2, He, CH4 |

