# Planned features and wild ideas

These tasks lists are not prioritized, just bunch of ideas in semi-random order.

- [x] Basic text content rendering
- [ ] Beautiful presentation for "esoteric" content
    - [x] code
    - [x] images
    - [x] basic tables (not `display: block`-styled)
    - [x] lists
    - [ ] video (inserted with raw HTML)
    - [ ] audio (inserted with raw HTML)
    - [ ] nested blockquotes
- [ ] Usable for code repositories from root of the repository
    - [ ] Handle `README.md` as alternative for `index.md`
    - [x] Copy only files/assets that are referenced from Markdown documents to the
          output
- [ ] Custom styling
   - [ ] Easy color overrides
   - [ ] Extending default styling
   - [ ] Option to use only user provided styling
- [ ] Index page generation for directory if not written by user
    - If no `index.md` is present, generate `index.html` which has
      link and description for every page in current directory.
    - This requires extracting `h1` + `h1 > p` from the content
- [ ] Handle dead links between content pages
    - Specific link rendering for dead links?
    - Have "404" content for dead links?
- [ ] Helpful tool output
    - [ ] Notify about dead links between documents
    - [ ] Notify about references to non-existent assets (images, videos, etc.)
    - [ ] Notify about documents that are not referenced from other documents
- [ ] Atom/RSS feed generation
    - As this is wiki, automatically generated "post published" feeds make little
      sense
    - Telling readers about significant content updates is useful though
    - Is single feed enough for whole wiki or is there need for feed per
      document?
- [ ] Handle HTML content
    - [ ] Include plain HTML pages to output via links
    - [ ] Document (so, go through) inline HTML in Markdown
    - [ ] Handle asset references in same way as in Markdown
        - This requires parsing all the HTML (standalone files as well as
          inline HTML) to find references

## Everything that requires building (and maintaining) our own HTML generator

See <https://github.com/pulldown-cmark/pulldown-cmark/issues/645> for details.
Basically pulldown-cmark (understandably) only wants to maintain minimal
implementation required for outputting HTML for CommonMark compatible markdown.

- [ ] Generate `<audio>` for "image" with audio mime-type
- [ ] Generate `<video>` for "image" with video mime-type
- [ ] Generate `<picture>`
- [ ] Look into if TreeSitter can be used to generate syntax highlighting for
      code blocks
- [ ] Generate anchor links for all the headings
- [ ] Wiki-style links `[[./subdir/page]]`
- [ ] Generate task lists in such a way that we can apply `list-style: none` to
      them

